FROM ubuntu
RUN apt-get update -y
RUN apt-get install tzdata -y
RUN apt-get install apache2 -y
ADD . /var/www/html
ENV name DevOps
ENTRYPOINT apachectl -D FOREGROUND
